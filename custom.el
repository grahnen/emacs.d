(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(LaTeX-command "latex --shell-escape")
 '(LaTeX-command-style (quote (("" "pdflatex -shell-escape %S%(PDFout)"))))
 '(agda2-highlight-face-groups nil)
 '(company-idle-delay 0.1)
 '(company-minimum-prefix-length 1)
 '(company-require-match nil)
 '(company-show-numbers nil)
 '(company-tooltip-align-annotations t)
 '(coq-compiler "coqc")
 '(custom-safe-themes
   (quote
    ("8db4b03b9ae654d4a57804286eb3e332725c84d7cdab38463cb6b97d5762ad26" "a8245b7cc985a0610d71f9852e9f2767ad1b852c2bdea6f4aadc12cce9c4d6d0" "0598c6a29e13e7112cfbc2f523e31927ab7dce56ebb2016b567e1eff6dc1fd4f" default)))
 '(dired-filter-group-saved-groups
   (quote
    (("Default"
      ("PDF"
       (extension . "pdf"))
      ("LaTeX"
       (extension "tex" "bib"))
      ("Org"
       (extension . "org"))
      ("Archives"
       (extension "zip" "rar" "tar" "gz" "bz2"))))))
 '(font-lock-add-keywords (quote org-mode) t)
 '(frame-background-mode (quote dark))
 '(global-company-mode t)
 '(haskell-process-type (quote auto))
 '(ivy-use-virtual-buffers t)
 '(kill-whole-line t)
 '(latex-run-command "latex --shell-escape")
 '(load-prefer-newer t)
 '(org-agenda-files (quote ("~/Org/Exjobbspresentation.org")))
 '(org-confirm-babel-evaluate nil)
 '(org-directory "~/Org")
 '(org-ellipsis "  ")
 '(org-export-hide-leading-stars t t)
 '(org-file-apps
   (quote
    ((auto-mode . emacs)
     ("\\.mm\\'" . default)
     ("\\.x?html?\\'" . default)
     ("\\.pdf\\'" . "okular --unique %s"))))
 '(org-fontify-done-headline t)
 '(org-fontify-quote-and-verse-blocks t)
 '(org-fontify-whole-heading-line t)
 '(org-hide-emphasis-markers t)
 '(org-latex-listings (quote minted))
 '(org-latex-packages-alist (quote (("cache=false" "minted" t))))
 '(org-latex-pdf-process (quote ("lualatex -shell-escape -output-directory %o %f")))
 '(org-ref-bibliography-notes "~/Nextcloud/bibliography/notes.org")
 '(org-ref-default-bibliography (quote ("~/Nextcloud/bibliography/references.bib")))
 '(org-ref-pdf-directory "~/Nextcloud/bibliography/bibtex-pdfs/")
 '(org-src-fontify-natively t)
 '(org-startup-folded (quote overview))
 '(org-startup-indented t)
 '(org-support-shift-select t)
 '(package-selected-packages
   (quote
    (doom-modeline yasnippet-snippets workgroups2 which-key visual-regexp-steroids use-package undo-tree slime pcre2el outshine org-ref org-plus-contrib org-mind-map org-download org-bullets minizinc-mode magit lua-mode lsp-ui lsp-haskell flycheck-rust dracula-theme dired-subtree dired-filter cquery counsel-projectile company-quickhelp company-lsp company-box cargo browse-kill-ring avy auctex all-the-icons-dired)))
 '(read-buffer-completion-ignore-case t)
 '(read-file-name-completion-ignore-case t)
 '(reftex-default-bibliography (quote ("~/Nextcloud/bibliography/references.bib")))
 '(safe-local-variable-values
   (quote
    ((coq-prog-name . "./HoTT/hoqtop")
     (eval let
	   ((default-directory
	      (locate-dominating-file buffer-file-name ".dir-locals.el")))
	   (make-local-variable
	    (quote coq-prog-name))
	   (setq coq-prog-name "hoqtop"))
     (eval let
	   ((default-directory
	      (locate-dominating-file buffer-file-name ".dir-locals.el")))
	   (make-local-variable
	    (quote coq-prog-name))
	   (setq coq-prog-name
		 (expand-file-name "../hoqtop")))
     (eval let
	   ((unimath-topdir
	     (expand-file-name
	      (locate-dominating-file buffer-file-name "UniMath"))))
	   (setq fill-column 100)
	   (make-local-variable
	    (quote coq-use-project-file))
	   (setq coq-use-project-file nil)
	   (make-local-variable
	    (quote coq-prog-args))
	   (setq coq-prog-args
		 (\`
		  ("-emacs" "-noinit" "-indices-matter" "-type-in-type" "-w" "-notation-overridden" "-Q"
		   (\,
		    (concat unimath-topdir "UniMath"))
		   "UniMath")))
	   (make-local-variable
	    (quote coq-prog-name))
	   (setq coq-prog-name
		 (concat unimath-topdir "sub/coq/bin/coqtop"))
	   (make-local-variable
	    (quote before-save-hook))
	   (add-hook
	    (quote before-save-hook)
	    (quote delete-trailing-whitespace))
	   (modify-syntax-entry 39 "w")
	   (modify-syntax-entry 95 "w")
	   (if
	       (not
		(memq
		 (quote agda-input)
		 features))
	       (load
		(concat unimath-topdir "emacs/agda/agda-input")))
	   (if
	       (not
		(member
		 (quote
		  ("chimney" "╝"))
		 agda-input-user-translations))
	       (progn
		 (setq agda-input-user-translations
		       (cons
			(quote
			 ("chimney" "╝"))
			agda-input-user-translations))
		 (setq agda-input-user-translations
		       (cons
			(quote
			 ("==>" "⟹"))
			agda-input-user-translations))
		 (agda-input-setup)))
	   (set-input-method "Agda"))
     (eval let
	   ((unimath-topdir
	     (concat
	      (expand-file-name
	       (locate-dominating-file buffer-file-name "coq"))
	      "UniMath/")))
	   (setq fill-column 100)
	   (make-local-variable
	    (quote coq-use-project-file))
	   (setq coq-use-project-file
		 (quote nil))
	   (make-local-variable
	    (quote coq-prog-args))
	   (setq coq-prog-args
		 (\`
		  ("-emacs" "-noinit" "-indices-matter" "-type-in-type" "-w" "-notation-overridden" "-Q"
		   (\,
		    (concat unimath-topdir "UniMath"))
		   "UniMath")))
	   (make-local-variable
	    (quote coq-prog-name))
	   (setq coq-prog-name
		 (concat unimath-topdir "sub/coq/bin/coqtop"))
	   (make-local-variable
	    (quote before-save-hook))
	   (add-hook
	    (quote before-save-hook)
	    (quote delete-trailing-whitespace))
	   (modify-syntax-entry 39 "w")
	   (modify-syntax-entry 95 "w")
	   (if
	       (not
		(memq
		 (quote agda-input)
		 features))
	       (load
		(concat unimath-topdir "emacs/agda/agda-input")))
	   (if
	       (not
		(member
		 (quote
		  ("chimney" "╝"))
		 agda-input-user-translations))
	       (progn
		 (setq agda-input-user-translations
		       (cons
			(quote
			 ("chimney" "╝"))
			agda-input-user-translations))
		 (setq agda-input-user-translations
		       (cons
			(quote
			 ("==>" "⟹"))
			agda-input-user-translations))
		 (agda-input-setup)))
	   (set-input-method "Agda")))))
 '(show-trailing-whitespace t)
 '(use-package-verbose nil)
 '(vr/engine (quote pcre2el) t)
 '(wg-prefix-key "" t))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:inherit nil :stipple nil :background "#282a36" :foreground "#f8f8f2" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 128 :width normal :foundry "CTDB" :family "Hack")))))
